    app.controller('accountCtrl', accountCtrl, ['$scope','$window', 'UserService', 'FlashService']);

    function accountCtrl($scope, $window, UserService, FlashService) {
        

        $scope.user = null;
        $scope.saveUser = saveUser;
        $scope.deleteUser = deleteUser;

        initController();

        function initController() {
            // get current user
            UserService.GetCurrent().then(function (user) {
                $scope.user = user;
            });
        }

        function saveUser() {
            UserService.Update($scope.user)
                .then(function () {
                    FlashService.Success('User updated');
                })
                .catch(function (error) {
                    FlashService.Error(error);
                });
        }

        function deleteUser() {
            UserService.Delete($scope.user._id)
                .then(function () {
                    // log user out
                    $window.location = '/login';
                })
                .catch(function (error) {
                    FlashService.Error(error);
                });
        }
    }