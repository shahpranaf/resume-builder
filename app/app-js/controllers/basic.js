/* Basic Info Controller */

app.controller('basicCtrl', basicCtrl, ['$scope', 'UserService', 'FlashService']);


function basicCtrl( $scope, UserService, FlashService){
    UserService.GetCurrent().then(function (user) {
            $scope.user =  user;
            $scope.basics = user.basics;
    });
    
    $scope.isMakingChanges = false;
    
    $scope.makingChanges = function(){
        if( $scope.isMakingChanges ){
            $('#save_basic').html('Save Changes');
            $('#save_basic').removeClass('btn-success');
        }
        else{
            return;
        }
    }
    
    
    $scope.basicInit = function(){
        if( basics.name ){
            $('#save_basic').html('Saved');
            $('#save_basic').addClass('btn-success');
            $scope.isMakingChanges = true;
        }
    }
    
    $scope.saveBasic = function(){
            $('#save_basic').html('Saved');
            $('#save_basic').addClass('btn-success');
            $scope.isMakingChanges = true;
            //$storage.localBasics = $scope.basics;
            $scope.user.basics = $scope.basics;
            console.log($scope.user);
            UserService.Update($scope.user)
                .then(function () {
                    FlashService.Success('User updated');
                })
                .catch(function (error) {
                    FlashService.Error(error);
                });
    }
}
