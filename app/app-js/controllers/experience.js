/* Experience Info Controller */

app.controller('experienceCtrl', experienceCtrl, ['$scope', 'UserService', 'FlashService']);

function experienceCtrl( $scope, UserService, FlashService, $timeout ){
    
    $scope.jobs = [];
    
    UserService.GetCurrent().then(function (user) {
            $scope.user =  user;
            $scope.jobs = ( typeof user.jobs !== 'undefined' && user.jobs.length !== 0 ) ? user.jobs : [];
    });
    
    $scope.saveExperience = function(){
        
        if( !$scope.jobEmployerTitle || !$scope.jobEmployerRole ){
            $('#save_experience').addClass('btn-danger').text("Missing required fields");
            FlashService.Error("Missing required fields");
            
            $('.required').each( function(){
               if( $(this).val() == '' ){
                   $(this).focus().parent('.form-group').addClass('has-error');
                   return false;
               } 
            });
            
            resetSaveExperience( $timeout );
            return;
        }
        console.log($scope.jobs);
        $scope.jobs.push( { jobEmployer : $scope.jobEmployerTitle, jobLocation: $scope.jobEmployerLocation, 
                            jobRole: $scope.jobEmployerRole, jobDuration: $scope.jobEmployerDuration,
                            jobDescription: $scope.jobEmployerDescription
        });
        
        //$storage.localJobs = $scope.jobs;
        $scope.user.jobs = $scope.jobs;
        UserService.Update($scope.user)
                .then(function () {
                    FlashService.Success('Experience updated');
                })
                .catch(function (error) {
                    FlashService.Error(error);
                });
        
        $('#save_experience').addClass('btn-success').text("Saved Successfully to List");
        
        resetSaveExperience( $timeout );
        
        /* REset Form */
         $scope.jobEmployerTitle = $scope.jobEmployerLocation = $scope.jobEmployerRole = $scope.jobEmployerDuration = $scope.jobEmployerDescription = '';
          
    }
    
    $scope.deleteEmployer = function($index){
        $scope.jobs.splice($index, 1);
        $scope.user.jobs = $scope.jobs;
        UserService.Update($scope.user)
                .then(function () {
                    FlashService.Success('Employer Deleted Successfully.');
                })
                .catch(function (error) {
                    FlashService.Error(error);
                });
    }
    
    $scope.editEmployer = function($index){
        $scope.jobEmployerTitle = $scope.jobs[$index].jobEmployer;
        $scope.jobEmployerLocation = $scope.jobs[$index].jobLocation;
        $scope.jobEmployerRole = $scope.jobs[$index].jobRole;
        $scope.jobEmployerDuration = $scope.jobs[$index].jobDuration;
        $scope.jobEmployerDescription = $scope.jobs[$index].jobDescription;
        $scope.jobs.splice($index,1);
    }
}

function resetSaveExperience( $timeout ){
    $timeout( function(){
                $('#save_experience').removeClass('btn-danger').removeClass('btn-success').text("Save To Experience List");
    }, 2000 );
}