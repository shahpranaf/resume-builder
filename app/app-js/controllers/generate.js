/* Generate Info Controller */

app.controller('generateCtrl', generateCtrl, ['$scope', 'UserService']);


function generateCtrl( $scope, $http, UserService ) {
    
    UserService.GetCurrent().then(function (user) {
            $scope.user =  user;
            $scope.basics = $scope.user.basics;
            $scope.summary =  $scope.user.summary;
            $scope.jobs =  $scope.user.jobs;
            $scope.projects =  $scope.user.projects;
            $scope.qualifications =  $scope.user.qualifications;
            $scope.skills =  $scope.user.skills;
    });
    
       
   $scope.generate_pdf = function() {
       $scope.lightbox = true;
       $scope.instructions = true;
       $scope.instructionsText = true;
   }
   $scope.closeInstructions = function() {
       $scope.lightbox = false;
       $scope.instructions = false;
       $scope.instructionsText = false;
   }
   
   // Actual Printing
    $scope.printResume = function(){
        $scope.showingResume = true;
        $scope.showingCoverLetter = true;
        $(document).prop('title', $scope.user.firstName+"_"+$scope.user.lastName+"_"+$scope.user.username+"_resume");
        print();
    }
}
