/* Home Controller */

app.controller('mainCtrl', mainCtrl, ['$scope', 'UserService']);
app.controller('navCtrl', navCtrl, ['$scope'])

function mainCtrl( $scope, UserService ){
    $scope.user = null;
    UserService.GetCurrent().then(function (user) {
            $scope.user =  user;
    });
    
    
    $scope.tinymceOptions = {
        plugins: 'link image code',
        //toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
    };
}


/* Controller Function
 * for Navigation
 */
function navCtrl( $scope, $location ){
    $scope.navClass = function(page){
        var currentPage = $location.path().substring(1) || 'home';
        return page === currentPage ? 'active' : '';
    }
}