/* project Info Controller */

app.controller('projectsCtrl', projectsCtrl, ['$scope', 'UserService', 'FlashService']);

function projectsCtrl( $scope, UserService, FlashService, $timeout ){
    $scope.projects = [];
    
    UserService.GetCurrent().then(function (user) {
            $scope.user =  user;
            $scope.projects = ( typeof user.projects !== 'undefined' && user.projects.length !== 0 ) ? user.projects : [];
    });
    
    $scope.saveProject = function(){
        
        if( !$scope.projectTitle || !$scope.projectRole ){
            $('#save_project').addClass('btn-danger').text("Missing required fields");
            FlashService.Error("Missing required fields");
            $('.required').each( function(){
               if( $(this).val() == '' ){
                   $(this).focus().parent('.form-group').addClass('has-error');
                   return false;
               } 
            });
            
            resetSaveProject( $timeout );
            return;
        }
        
        $scope.projects.push( { projectTitle : $scope.projectTitle, projectDescription: $scope.projectDescription, 
                            projectRole: $scope.projectRole, projectUrl: $scope.projectUrl
        });
        
        //$storage.localProjects = $scope.projects;
        
        $scope.user.projects = $scope.projects;console.log($scope.user.projects);
        UserService.Update($scope.user)
                .then(function () {
                    FlashService.Success('Project updated Successfully');
                })
                .catch(function (error) {
                    FlashService.Error(error);
                });
        
        $('#save_project').addClass('btn-success').text("Saved Successfully to List");
        
        resetSaveProject( $timeout );
        
        /* REset Form */
         $scope.projectTitle =$scope.projectUrl = $scope.projectRole = $scope.projectDescription = '';
          
    }
    
    $scope.deleteProject = function($index){
        $scope.projects.splice($index, 1);
        $scope.user.projects = $scope.projects;
        UserService.Update($scope.user)
                .then(function () {
                    FlashService.Success('Employer Deleted Successfully.');
                })
                .catch(function (error) {
                    FlashService.Error(error);
                });
    }
    
    $scope.editProject = function($index){
        $scope.projectTitle = $scope.projects[$index].projectTitle;
        $scope.projectUrl = $scope.projects[$index].projectUrl;
        $scope.projectRole = $scope.projects[$index].projectRole;
        $scope.projectDescription = $scope.projects[$index].projectDescription;
        $scope.projects.splice($index,1);
    }
}

function resetSaveProject( $timeout ){
    $timeout( function(){
                $('#save_project').removeClass('btn-danger').removeClass('btn-success').text("Save To Project List");
    }, 2000 );
}