/* Skill Info Controller */

app.controller('skillsCtrl', skillsCtrl, ['$scope', 'UserService', 'FlashService']);

function skillsCtrl( $scope, UserService, FlashService, $timeout ){
   $scope.skills = [];
    
    UserService.GetCurrent().then(function (user) {
            $scope.user =  user;
            $scope.skills = ( typeof user.skills !== 'undefined' && user.skills.length !== 0 ) ? user.skills : [];
    });
    
    $scope.saveSkills = function(){
        
        if( !$scope.skill ){
            $('#save_skill').addClass('btn-danger').text("Missing required fields");
            $('.required').each( function(){
               if( $(this).val() == '' ){
                   $(this).focus().parent('.form-group').addClass('has-error');
                   return false;
               } 
            });
            
            resetSaveSkill( $timeout );
            return;
        }
        
        $scope.skills.push( { skill : $scope.skill });
        
        //$storage.localSkills = $scope.skills;
        $scope.user.skills = $scope.skills;
        UserService.Update($scope.user)
                .then(function () {
                    FlashService.Success('Skills Added Successfully');
                })
                .catch(function (error) {
                    FlashService.Error(error);
                });
        
        $('#save_skill').addClass('btn-success').text("Saved Successfully to List");
        
        resetSaveSkill( $timeout );
        
        /* REset Form */
         $scope.skill = '';
          
    }
    
    $scope.deleteSkill = function($index){
        $scope.skills.splice($index, 1);
        
        $scope.user.skills = $scope.skills;
        UserService.Update($scope.user)
                .then(function () {
                    FlashService.Success('Skills Deleted Successfully');
                })
                .catch(function (error) {
                    FlashService.Error(error);
                });
    }
    
    $scope.editSkill = function($index){
        $scope.skill = $scope.skills[$index].skill;
        $scope.skills.splice($index,1);
    }
}

function resetSaveSkill( $timeout ){
    $timeout( function(){
                $('#save_skill').removeClass('btn-danger').removeClass('btn-success').text("Save To Skill List");
    }, 2000 );
}