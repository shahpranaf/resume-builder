/* Summary Info Controller */

app.controller('summaryCtrl', summaryCtrl, ['$scope', 'UserService', 'FlashService']);


function summaryCtrl( $scope, UserService, FlashService ){
    $scope.summary = '';
    UserService.GetCurrent().then(function (user) {
            $scope.user =  user;
            $scope.summary = user.summary;
    });
    
    $scope.isMakingChanges = false;
    
    $scope.makingChanges = function(){
        if( $scope.isMakingChanges ){
            $('#save_summary').html('Save Summary');
            $('#save_summary').removeClass('btn-success');
        }
        else{
            return;
        }
    }
    
    
    $scope.summaryInit = function(){
        if( summary.body ){
            $('#save_summary').html('Saved');
            $('#save_summary').addClass('btn-success');
            $scope.isMakingChanges = true;
        }
    }
    
    $scope.saveSummary = function(){
            $('#save_summary').html('Saved');
            $('#save_summary').addClass('btn-success');
            $scope.isMakingChanges = true;
            //$storage.localSummary = $scope.summary;
            $scope.user.summary = $scope.summary;
            
            UserService.Update($scope.user)
                .then(function () {
                    FlashService.Success('Summary updated');
                })
                .catch(function (error) {
                    FlashService.Error(error);
                });
    }
}
