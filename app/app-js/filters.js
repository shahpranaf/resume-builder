app.filter('renderHTMLCorrectly', renderHTML, ['$sce'] );

function renderHTML($sce)
{
	return function(stringToParse)
	{
		return $sce.trustAsHtml(stringToParse);
	}
};