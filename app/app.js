
    var app = angular.module("app", ['ui.router', 'ngStorage', 'ui.tinymce']);
    
    app.config( function($stateProvider, $urlRouterProvider) {
        // default route
        $urlRouterProvider.otherwise("/");

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'view/home.html',
                controller: 'mainCtrl',
            })
            .state('basic', {
                url: '/basic',
                templateUrl: 'view/basic.html',
                controller: 'basicCtrl',
            })
            .state('summary', {
                url: '/summary',
                templateUrl: 'view/summary.html',
                controller: 'summaryCtrl',
            })
            .state('experience', {
                url: '/experience',
                templateUrl: 'view/experience.html',
                controller: 'experienceCtrl',
            })
            .state('projects', {
                url: '/projects',
                templateUrl: 'view/projects.html',
                controller: 'projectsCtrl',
            })
            .state('qualifications', {
                url: '/qualifications',
                templateUrl: 'view/qualifications.html',
                controller: 'qualificationsCtrl',
            })
            .state('skills', {
                url: '/skills',
                templateUrl: 'view/skills.html',
                controller: 'skillsCtrl',
            })
            .state('generate', {
                url: '/generate',
                templateUrl: 'view/generate.html',
                controller: 'generateCtrl',
            })
            .state('account', {
                url: '/account',
                templateUrl: 'view/account.html',
                controller: 'accountCtrl',
            });
        /*$routeProvider
        
        .when( '/', {
            templateUrl: 'view/home.html',
            controller: 'mainCtrl'
        })
        
        .when( '/basic', {
            templateUrl: 'view/basic.html',
            controller: 'basicCtrl'
        })
        
        .when( '/summary', {
            templateUrl: 'view/summary.html',
            controller: 'summaryCtrl'
        })
        
        .when( '/experience', {
            templateUrl: 'view/experience.html',
            controller: 'experienceCtrl'
        })
        
        .when( '/projects', {
            templateUrl: 'view/projects.html',
            controller: 'projectsCtrl'
        })
        
        .when( '/qualifications', {
            templateUrl: 'view/qualifications.html',
            controller: 'qualificationsCtrl'
        })
        
        .when( '/skills', {
            templateUrl: 'view/skills.html',
            controller: 'skillsCtrl'
        })
        
        .when( '/generate', {
            templateUrl: 'view/generate.html',
            controller: 'generateCtrl'
        })*/
    } );

    app.run( function( $http, $rootScope, $window ) {
        // add JWT token as default auth header
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + $window.jwtToken;

        // update active tab on state change
        /*$rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            $rootScope.activeTab = toState.data.activeTab;
        });*/
    } );
    
    // manually bootstrap angular after the JWT token is retrieved from the server
   $(function () {
        // get JWT token from server
        $.get('/app/token', function (token) {
            window.jwtToken = token;

            angular.bootstrap(document, ['app']);
        });
    });